const Product = require("../models/product");
const Cart = require("../models/cart");

exports.getProducts = (req, res, next) => {
  Product.fetchAll().then(products => {
    res.render("shop/product-list", {
      prods: products,
      pageTitle: "All Products",
      path: "/products"
    });
  });
};

exports.getProduct = (req, res, next) => {
  const productId = req.params["productId"];
  Product.findById(productId).then(productData => {
    res.render("shop/product-detail", {
      path: "/products",
      pageTitle: "Product Detail",
      product: productData
    });
  });
};

exports.getIndex = (req, res, next) => {
  Product.fetchAll().then(products => {
    res.render("shop/index", {
      prods: products,
      pageTitle: "Shop",
      path: "/"
    });
  });
};

exports.getCart = (req, res, next) => {
  Promise.all([Cart.getCartInformation(), Product.fetchAll()]).then(data => {
    let cartInformation = data[0];
    let products = data[1];
    let cartProducts = [];
    for (product of products) {
      let cartProductData = cartInformation.products.find(
        prod => prod.id === product.id
      );
      if (cartProductData) {
        cartProducts.push({
          productData: product,
          qty: cartProductData.qty
        });
      }
    }
    res.render("shop/cart", {
      path: "/cart",
      pageTitle: "Your Cart",
      products: cartProducts
    });
  });
};

exports.postCartDeleteProduct = (req, res, next) => {
  const prodId = req.body.productId;
  Product.findById(prodId).then(product => {
    Cart.deleteProduct(prodId, product.price).then(result => {
      console.log("DELETE: cart updated", result);
      res.redirect("/cart");
    });
  });
};

exports.postCart = (req, res, next) => {
  const productId = req.body.productId;
  Product.findById(productId).then(product => {
    Cart.addProduct(productId, product.price);
    res.redirect("/cart");
  });
  console.log(productId);
};

exports.getOrders = (req, res, next) => {
  res.render("shop/orders", {
    path: "/orders",
    pageTitle: "Your Orders"
  });
};

exports.getCheckout = (req, res, next) => {
  res.render("shop/checkout", {
    path: "/checkout",
    pageTitle: "Checkout"
  });
};
