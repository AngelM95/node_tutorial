const path = require("path");
const fs = require("fs");

const rootDir = require("../util/path");

const p = path.join(rootDir, "data", "products.json");

const Cart = require("./cart");

const getProductsFromFile = new Promise((resolve, reject) => {
  fs.readFile(p, (err, fileContent) => {
    if (!err) {
      products = JSON.parse(fileContent);
      resolve(products);
    }
  });
});

module.exports = class Product {
  constructor(id, title, imageUrl, description, price) {
    this.id = id;
    this.title = title;
    this.imageUrl = imageUrl;
    this.description = description;
    this.price = price;
  }

  save() {
    let productArr = [];
    getProductsFromFile.then(products => {
      productArr = [...products];
      if (this.id) {
        const existingProductIndex = products.findIndex(
          prod => prod.id === this.id
        );
        if (existingProductIndex) {
          productArr[existingProductIndex] = this;
        }
      } else {
        this.id = Math.random().toString();
        productArr.push(this);
      }
      fs.writeFile(p, JSON.stringify(productArr), err => {});
    });
  }

  static deleteById(idProduct) {
    return new Promise((resolve, reject) => {
      let updatedProducts = [];
      getProductsFromFile
        .then(products => {
          updatedProducts = [...products];
          const productToRemove = updatedProducts.find(
            prod => prod.id === idProduct
          );
          updatedProducts = updatedProducts.filter(
            prod => prod.id !== productToRemove.id
          );

          fs.writeFile(p, JSON.stringify(updatedProducts), err => {
            if (!err) {
              Cart.deleteProduct(idProduct, productToRemove.price).then(res => {
                resolve(res);
              });
            }
          });
        })
        .catch(err => reject(err));
    });
  }

  // No instanciable
  static fetchAll() {
    return getProductsFromFile;
  }

  static findById(idProduct) {
    return new Promise((resolve, reject) => {
      getProductsFromFile.then(products => {
        let productFound = products.find(product => {
          return product.id === idProduct;
        });
        productFound ? resolve(productFound) : reject("Not products found");
      });
    });
  }
};
