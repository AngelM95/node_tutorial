const path = require("path");
const fs = require("fs");
const fsPromises = require("fs").promises; // or require('fs/promises') in v10.0.0

const rootDir = require("../util/path");
const productModel = require("./product");

const p = path.join(rootDir, "data", "cart.json");

const getCartInformationFromFile = new Promise((resolve, reject) => {
  fs.readFile(p, (err, fileContent) => {
    if (!err) {
      products = JSON.parse(fileContent);
      resolve(products);
    }
  });
});

module.exports = class Product {
  constructor() {
    this.products = [];
    this.totalPrice = 0;
  }

  static addProduct(id, productPrice) {
    getCartInformationFromFile.then(cartData => {
      let cart = { products: [], totalPrice: 0 };
      // set product data
      cart = cartData;
      const existingProductIndex = cart.products.findIndex(
        prod => prod.id === id
      );
      const existingProduct = cart.products[existingProductIndex];
      let updatedProduct = {};
      // Just exists
      if (existingProduct) {
        updatedProduct = { ...existingProduct };
        updatedProduct.qty = updatedProduct.qty + 1;
        cart.products = [...cart.products];
        cart.products[existingProductIndex] = updatedProduct;
      }
      // New product
      if (!existingProduct) {
        updatedProduct = { id: id, qty: 1 };
        cart.products = [...cart.products, updatedProduct];
      }
      cart.totalPrice = cart.totalPrice + +productPrice;

      fs.writeFile(p, JSON.stringify(cart), err => {
        console.log(err);
      });
    });
  }

  static deleteProduct(id, productPrice) {
    return new Promise((resolve, reject) => {
      getCartInformationFromFile.then(listProducts => {
        const updateCart = { ...listProducts };
        const product = updateCart.products.find(prod => prod.id === id);
        if (!product) {
          return;
        }
        const productQty = product.qty;

        updateCart.products = updateCart.products.filter(
          prod => id !== prod.id
        );
        updateCart.totalPrice =
          updateCart.totalPrice - productPrice * productQty;
        fsPromises
          .writeFile(p, JSON.stringify(updateCart))
          .then(res => {
            resolve(res);
          })
          .catch(err => reject(err));
      });
    });
  }

  static getCartInformation() {
    return getCartInformationFromFile;
  }
};
